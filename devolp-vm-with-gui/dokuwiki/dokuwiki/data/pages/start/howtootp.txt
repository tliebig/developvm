==== How to run OpenTripPlanner ====

We assume you first built the project at /vagrant/OpenTripPlanner-master first with [[start:howtoeclipse|eclipse]] or using the following lines
<code>
cd /vagrant/OpenTripPlanner-master
mvn compile
mvn package -DskipTests
</code>

Afterwards, a shell enter the following lines to start OTP
<code>
cd /vagrant/OpenTripPlanner-master/target
java -Xmx4G -jar otp-0.20.0-SNAPSHOT-shaded.jar --server --buildTp --build /vagrant/otp-warsaw/ 
</code>

These commands use the OpenStreetMap and GTFS files located at /vagrant/otp-warsaw

Point your browser (host machine) to http://localhost:8080 in order to access OTP GUI.

