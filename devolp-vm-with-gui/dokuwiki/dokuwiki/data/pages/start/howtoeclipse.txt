==== How to Start an X Session ====

To start an X Session from command line, log in with
<code>
user:vagrant
pass:vagrant
</code>

and enter:

<code>
startx
</code>

==== How to Start Eclipse ====
After starting the X Session, point the cursor in the toolbar at the bottom of the Screen, open an xterm and enter 
<code>
cd /vagrant/eclipse
./eclipse
</code>
