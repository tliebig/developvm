# VaVeL Develop VM

This repo provides an [ubuntu][ubuntu] Virtual Machine for developement of dynamic routing in the [VaVeL project][vavel].

![Screenshot](https://bitbucket.org/tliebig/developvm/raw/master/devolp-vm-with-gui/dokuwiki/dokuwiki/data/media/wiki/2017-08-14_10_36_34-devolp-vm-with-gui_default_1502462435395_93411_running_-_oracle_vm_virtualbox.png)

### Installation

Install [virtualbox][virtualbox] and [vagrant][vagrant] then bring the machine initially up with

```sh
 vagrant up
``` 

The files are mounted into the VM  at /vagrant

Stop machine with

```sh
 vagrant halt
``` 
 and resume with 
```sh
 vagrant reload
``` 
### Login Credentials
```sh
user: vagrant
pass: vagrant
```

### Usage
issue `vagrant ssh` from your host machine to connect to the dev machine

#### OTP test client

In order to run the OTP client that is used for research by TUDO (dynamic multimodal routing)

1. Go to `/vagrant/OpenTripPlanner-master/target` (compile if necessary, you'll find eclipse at `/vagrant/eclipse/.eclipse`)

2. Start the OTP instance 
```sh
java -Xmx4G -jar otp-0.20.0-SNAPSHOT-shaded.jar --server --build /vagrant/otp-warsaw/ --inMemory
```

In order to run the OTP client that is used for research by WUT (comparison of several schedules)

1. Go to `$HOME/otpsimpletestclient`

2. Start the OTP instances
```sh
startOTP.sh
```
and wait till they listen on appropriate ports - 8082, 8084, 8086. You will know they are ready by the console message.

3. Modify the client to what you need and run it
```sh
runClient.sh
```

### Wiki

A filesystem based [dokuwiki][dokuwiki] is included in the VM. You can connect to it via port 8800 (from your host system). Or start it directly from your host Windows commandline using the **start-dokuwiki** link.


> [Thomas Liebig][tliebig] <thomas@thomas-liebig.eu>

[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)

   [vavel]: <http://www.vavel-project.eu>
   [ubuntu]: <http://www.ubuntu.com/>
   [virtualbox]: <https://www.virtualbox.org/>
   [vagrant]: <https://www.vagrantup.com/>
   [dokuwiki]: <https://www.dokuwiki.org/dokuwiki>
   [tliebig]: <http://www.thomas-liebig.eu>
